<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\ContentReplacer\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use SGalinski\ContentReplacer\Repository\TermRepository;
use SGalinski\ContentReplacer\Service\AbstractParserService;
use SGalinski\ContentReplacer\Service\CustomParserService;
use SGalinski\ContentReplacer\Service\SpanParserService;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\TypoScriptAspect;
use TYPO3\CMS\Core\Http\ImmediateResponseException;
use TYPO3\CMS\Core\Http\StreamFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class ContentReplacerMiddleware
 *
 * @package SGalinski\ContentReplacer\Middleware
 */
class ContentReplacerMiddleware implements MiddlewareInterface {
	/**
	 * Extension Configuration
	 *
	 * @var array
	 */
	protected array $extensionConfiguration = [];

	/**
	 * @var TermRepository
	 */
	protected TermRepository $termRepository;

	/**
	 * Constructor: Initializes the internal class properties.
	 *
	 * Note: The extension configuration array consists of the global and typoscript configuration.
	 *
	 * @throws ImmediateResponseException
	 */
	public function __construct() {
		$this->extensionConfiguration = $this->prepareConfiguration();
		$this->termRepository = GeneralUtility::makeInstance(TermRepository::class);
	}

	/**
	 * @inheritDoc
	 */
	public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface {
		$response = $handler->handle($request);
		if (isset($this->extensionConfiguration['disable']) && $this->extensionConfiguration['disable']) {
			return $response;
		}

		$spanParser = $this->getSpanParser();
		$content = $this->parseAndReplace($spanParser, (string) $response->getBody());

		$specialWrapCharacter = trim($this->extensionConfiguration['specialParserCharacter'] ?? '');
		if ($specialWrapCharacter !== '') {
			$customParser = $this->getCustomParser($specialWrapCharacter);
			$content = $this->parseAndReplace($customParser, $content);
		}

		$streamFactory = GeneralUtility::makeInstance(StreamFactory::class);
		$stream = $streamFactory->createStream($content);

		return $response->withBody($stream);
	}

	/**
	 * Returns a span tag parser instance
	 *
	 * @return SpanParserService
	 * @throws \InvalidArgumentException
	 */
	protected function getSpanParser(): SpanParserService {
		$spanParser = GeneralUtility::makeInstance(SpanParserService::class);
		$spanParser->setExtensionConfiguration($this->extensionConfiguration);
		$spanParser->injectTermRepository($this->termRepository);

		return $spanParser;
	}

	/**
	 * Returns a custom wrap character parser instance
	 *
	 * @param string $specialWrapCharacter
	 * @return CustomParserService
	 * @throws \InvalidArgumentException
	 */
	protected function getCustomParser(string $specialWrapCharacter): CustomParserService {
		$customParser = GeneralUtility::makeInstance(CustomParserService::class);
		$customParser->setExtensionConfiguration($this->extensionConfiguration);
		$customParser->injectTermRepository($this->termRepository);
		$customParser->setWrapCharacter($specialWrapCharacter);

		return $customParser;
	}

	/**
	 * Parses and replaces the content several times until the given parser cannot find
	 * any more occurrences or the maximum amount of possible passes is reached.
	 *
	 * @param AbstractParserService $parser
	 * @param string $content
	 * @return string
	 */
	protected function parseAndReplace(AbstractParserService $parser, string $content): string {
		$loopCounter = 0;
		while (TRUE) {
			if ($loopCounter++ > ($this->extensionConfiguration['amountOfPasses'] ?? 0)) {
				break;
			}

			$occurrences = $parser->parse($content);
			if (!count($occurrences)) {
				break;
			}

			foreach ($occurrences as $category => $terms) {
				$content = $parser->replaceByCategory($category, $terms, $content);
			}
		}

		return $content;
	}

	/**
	 * Returns the merged extension configuration of the global configuration and the typoscript
	 * settings.
	 *
	 * @return array
	 * @throws ImmediateResponseException
	 */
	public function prepareConfiguration(): array {
		$extensionConfiguration = $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['content_replacer'] ?? [];

		if (empty($GLOBALS['TSFE']->tmpl->setup)) {
			// we need the TypoScript configuration here, force parsing
			GeneralUtility::makeInstance(Context::class)->setAspect(
				'typoscript',
				GeneralUtility::makeInstance(TypoScriptAspect::class, TRUE)
			);
			$GLOBALS['TSFE']->tmpl->start($GLOBALS['TSFE']->rootLine);
		}

		$typoscriptConfiguration = $GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_content_replacer.'] ?? [];

		if (is_array($typoscriptConfiguration)) {
			foreach ($typoscriptConfiguration as $key => $value) {
				$extensionConfiguration[$key] = $value;
			}
		}

		return $extensionConfiguration;
	}
}
