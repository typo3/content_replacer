<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\ContentReplacer\Service;

use Doctrine\DBAL\Exception;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use TYPO3\CMS\Core\Log\Logger;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Substitution service that parses and replaces special span tags inside the code
 */
class SpanParserService extends AbstractParserService {
	/**
	 * This function parses the generated content from TYPO3 and returns an ordered list
	 * of terms with their related categories.
	 *
	 * Structure:
	 *
	 * category1
	 * |-> term1
	 * |-> term2
	 * category2
	 * |-> term1
	 * ...
	 *
	 * Each term has some additional properties:
	 * - pre: attributes before the class attribute
	 * - post: attributes after the class attribute
	 * - classAttribute: the class attribute without the replacement class
	 *
	 * @param string $content
	 * @return array
	 */
	public function parse(string $content): array {
		$matches = [];
		$prefix = (string) ($this->extensionConfiguration['prefix'] ?? '');
		if (ExtensionManagementUtility::isLoaded('headless')) {
			// when headless is present, content will be escaped (JSON output), so we got to add some small details:
			/** @noinspection RegExpRedundantEscape */
			$pattern = '/' .
				'<span' . // This expression includes any span nodes and parses
				'(?=[^>]+' . // any attributes of the beginning start tag.
				'(?=(class=\\\\"([^"]*?' . preg_quote($prefix, '/') . '[^"]+?)\\\\"))' .
				')' . // Use only spans which start with the defined class prefix
				' (.*?)\1(.*?)>' . // and stop if the closing character is reached.
				'(.*?)<\\\\\\/span>' . // Finally we fetch the span content!
				'/is';
		} else {
			/** @noinspection RegExpRedundantEscape */
			$pattern = '/' .
				'<span' . // This expression includes any span nodes and parses
				'(?=[^>]+' . // any attributes of the beginning start tag.
				'(?=(class="([^"]*?' . preg_quote($prefix, '/') . '[^"]+?)"))' .
				')' . // Use only spans which start with the defined class prefix
				' (.*?)\1(.*?)>' . // and stop if the closing character is reached.
				'(.*?)<\/span>' . // Finally we fetch the span content!
				'/is';
		}

		preg_match_all($pattern, $content, $matches);
		$categories = [];
		foreach ($matches[5] as $index => $term) {
			$term = trim($term);

			$category = '';
			$classes = explode(' ', $matches[2][$index]);
			foreach ($classes as $classIndex => $class) {
				$class = trim($class);

				if ($prefix !== '' && str_contains($class, $prefix)) {
					$category = str_replace($prefix, '', $class);
					unset($classes[$classIndex]);
					break;
				}
			}

			if ($category === '') {
				$logger = GeneralUtility::makeInstance(Logger::class, __CLASS__);
				$logger->warning('(content_replacer) Incorrect match: ' . implode(' ', $classes));
				continue;
			}

			$categories[$category][$term]['pre'] = $matches[3][$index];
			$categories[$category][$term]['post'] = $matches[4][$index];

			$categories[$category][$term]['classAttribute'] = '';
			$otherClasses = implode(' ', $classes);
			if ($otherClasses !== '') {
				$categories[$category][$term]['classAttribute'] = 'class="' . $otherClasses . '"';
			}
		}

		return $categories;
	}

	/**
	 * Replaces the given terms with their related replacement values.
	 *
	 * @param string $category
	 * @param array $terms
	 * @param string $content
	 * @return string
	 * @throws Exception
	 * @throws AspectNotFoundException
	 */
	public function replaceByCategory(string $category, array $terms, string $content): string {
		$search = $replace = [];
		$defaultReplacement = $this->prepareFoundTerms($terms, $category);
		foreach ($terms as $termName => $term) {
			if (!isset($term['uid'])) {
				$term = array_merge((array) $term, $defaultReplacement);
				$term['term'] = $termName;
			}

			$searchClass = preg_quote($this->extensionConfiguration['prefix'] . $category, '/');
			if (ExtensionManagementUtility::isLoaded('headless')) {
				// when EXT:headless is present, content will be escaped (JSON output), so we got to add some small details:
				$search[$termName] = '/' .
					'<span ' . preg_quote($term['pre'] ?? '', '/') .
					'class=\\\\"([^"]*?)' . $searchClass . '([^"]*?)\\\\"' .
					preg_quote($term['post'], '/') . '>' .
					'\s*?' . preg_quote($term['term'], '/') . '\s*?' .
					'<\\\\\\/span>' .
					'/i';
			} else {
				$search[$termName] = '/' .
					'<span ' . preg_quote($term['pre'] ?? '', '/') .
					'class="([^"]*?)' . $searchClass . '([^"]*?)"' .
					preg_quote($term['post'] ?? '', '/') . '>' .
					'\s*?' . preg_quote($term['term'] ?? '', '/') . '\s*?' .
					'<\/span>' .
					'/i';
			}

			$replace[$termName] = $this->prepareReplacementTerm(
				$term['replacement'] ?? '',
				trim($term['stdWrap'] ?? ''),
				$termName
			);

			if (trim($term['pre'] ?? '') !== '' || trim($term['post'] ?? '') !== '' || trim($term['classAttribute'] ?? '') !== '') {
				$attributes = trim($term['pre'] . ' ' . $term['post'] . ' ' . $term['classAttribute']);
				$replace[$termName] = '<span ' . $attributes . '>' . $replace[$termName] . '</span>';
			}
		}

		return preg_replace($search, $replace, $content);
	}
}
