<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\ContentReplacer\Service;

use Doctrine\DBAL\Exception;
use SGalinski\ContentReplacer\Repository\TermRepository;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use TYPO3\CMS\Core\Http\ApplicationType;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\BackendConfigurationManager;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/**
 * Abstract parser that handles the parsing and replacement of terms
 */
abstract class AbstractParserService {
	/**
	 * Extension Configuration
	 *
	 * @var array
	 */
	protected array $extensionConfiguration = [];

	/**
	 * lib.parseFunc_RTE configuration
	 *
	 * @var array
	 */
	protected array $parseFunc = [];

	/**
	 * @var TermRepository
	 */
	protected TermRepository $termRepository;

	/**
	 * Constructor: Initializes the internal class properties.
	 *
	 * Note: The extension configuration array consists of the global and typoscript configuration.
	 */
	public function __construct() {
		$this->parseFunc = $GLOBALS['TSFE']->tmpl->setup['lib.']['parseFunc_RTE.'] ?? [];
	}

	/**
	 * Sets the extension configuration
	 *
	 * @param array $extensionConfiguration
	 * @return void
	 */
	public function setExtensionConfiguration(array $extensionConfiguration): void {
		$this->extensionConfiguration = $extensionConfiguration;
	}

	/**
	 * Injects the term repository
	 *
	 * @param TermRepository $repository
	 * @return void
	 */
	public function injectTermRepository(TermRepository $repository): void {
		$this->termRepository = $repository;
	}

	/**
	 * Prepares a replacement value by applying the possible stdWrap and executing RTE
	 * transformations. The stdWrap typoscript object must be defined inside the extension
	 * namespace "plugin.tx_content_replacer".
	 *
	 * Note: If the replacement text is empty, we pass the term name as the initial content of the
	 * stdWrap object.
	 *
	 * @param string $replacement
	 * @param string $stdWrap
	 * @param string $termName
	 * @return string
	 */
	protected function prepareReplacementTerm(string $replacement, string $stdWrap, string $termName): string {
		$cObject = GeneralUtility::makeInstance(ContentObjectRenderer::class);

		if ($replacement !== '') {
			$replacement = preg_replace(
				'/^<p>(.+)<\/p>$/s',
				'\1',
				$cObject->parseFunc($replacement, $this->parseFunc)
			);
		}

		if ($stdWrap !== '') {
			if (ApplicationType::fromRequest($GLOBALS['TYPO3_REQUEST'])->isBackend()) {
				$configurationManager = GeneralUtility::makeInstance(BackendConfigurationManager::class);
				$extbaseFrameworkConfiguration = $configurationManager->getTypoScriptSetup();
				$typoscriptConfiguration =
					$extbaseFrameworkConfiguration['plugin.']['tx_content_replacer.'][$stdWrap . '.'] ?? [];
			} else {
				$typoscriptConfiguration =
					$GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_content_replacer.'][$stdWrap . '.'] ?? [];
			}

			if (count($typoscriptConfiguration)) {
				$replacement = $cObject->stdWrap(
					($replacement === '' ? $termName : $replacement),
					$typoscriptConfiguration
				) ?? '';
			}
		}

		return $replacement;
	}

	/**
	 * Enriches the given terms with information's from the database and returns the default
	 * term for any replacements.
	 *
	 * @param array $terms
	 * @param string $category
	 * @return array
	 * @throws Exception
	 * @throws AspectNotFoundException
	 */
	protected function prepareFoundTerms(array &$terms, string $category): array {
		$terms['*'] = [];
		$termNames = array_keys($terms);
		$storagePageIds = GeneralUtility::intExplode(',', $this->extensionConfiguration['storagePid'], TRUE);

		$configuredTerms = $this->termRepository->fetchTerms($termNames, $category, $storagePageIds);
		$terms = array_merge_recursive($terms, $configuredTerms);
		if (!isset($terms['*']['stdWrap'])) {
			$terms['*']['stdWrap'] = $category;
		}

		return $terms['*'];
	}

	/**
	 * This function parses the generated content from TYPO3 and returns an ordered list
	 * of terms with their related categories.
	 *
	 * @abstract
	 * @param string $content
	 * @return array
	 */
	abstract public function parse(string $content): array;

	/**
	 * Replaces the given terms with their related replacement values.
	 *
	 * @abstract
	 * @param string $category
	 * @param array $terms
	 * @param string $content
	 * @return string
	 */
	abstract public function replaceByCategory(string $category, array $terms, string $content): string;
}
