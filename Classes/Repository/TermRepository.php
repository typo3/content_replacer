<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\ContentReplacer\Repository;

use Doctrine\DBAL\ArrayParameterType;
use Doctrine\DBAL\Exception;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\Exception\AspectNotFoundException;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Domain\Repository\PageRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Repository for fetching terms
 */
class TermRepository {
	/**
	 * Returns the given terms with their related information's.
	 *
	 * @param array $filterTerms
	 * @param string $category
	 * @param array $storagePageIds
	 * @return array
	 * @throws Exception
	 * @throws AspectNotFoundException
	 */
	public function fetchTerms(array $filterTerms, string $category, array $storagePageIds): array {
		$context = GeneralUtility::makeInstance(Context::class);
		$connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
		$queryBuilder = $connectionPool->getQueryBuilderForTable('tx_content_replacer_term');
		$queryBuilder->select(
			'term.uid',
			'term.pid',
			'term',
			'replacement',
			'stdWrap',
			'category_uid',
			'sys_language_uid'
		)
			->from('tx_content_replacer_term', 'term')
			->leftJoin(
				'term',
				'tx_content_replacer_category',
				'category',
				'category.uid = term.category_uid'
			)->where(
				$queryBuilder->expr()->and(
					$queryBuilder->expr()->in(
						'term',
						$queryBuilder->createNamedParameter($filterTerms, ArrayParameterType::STRING)
					),
					$queryBuilder->expr()->in('term.sys_language_uid', [-1, 0]),
					$queryBuilder->expr()->eq(
						'category',
						$queryBuilder->createNamedParameter($category)
					)
				)
			);
		if (count($storagePageIds) > 0) {
			$queryBuilder->andWhere(
				$queryBuilder->expr()->in(
					'term.pid',
					$queryBuilder->createNamedParameter($storagePageIds, ArrayParameterType::INTEGER)
				)
			);
		}

		$results = $queryBuilder->executeQuery()->fetchAllAssociative();
		$terms = [];
		$pageRepository = GeneralUtility::makeInstance(PageRepository::class);
		$languageAspect = $context->getAspect('language');
		$sysLanguageId = $languageAspect->getId();
		foreach ($results as $term) {
			if ($sysLanguageId > 0) {
				// This bit of code is bad performancewise, we should consider fetching relevant records via
				// one single db call instead of looping through found default language records and fetching overlays
				// for each of these separately
				$term = $pageRepository->getLanguageOverlay(
					'tx_content_replacer_term',
					$term,
					$languageAspect
				);
			}

			$terms[$term['term']] = $term;
		}

		return $terms;
	}
}
