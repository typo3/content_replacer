<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "content_replacer".
 *
 * Auto generated 12-04-2016 23:08
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF['content_replacer'] = [
	'title' => 'Content Replacer',
	'description' => 'You need a fast substitution of terms with full support of typoscript, categories and RTE integration? If yes, the extension could be perfectly fit into your project. The performance is gained by wrapping of the replacement terms to simplify the parsing process.',
	'category' => 'fe',
	'version' => '8.0.2',
	'state' => 'stable',
	'author' => 'Stefan Galinski',
	'author_email' => 'stefan.galinski@gmail.com',
	'author_company' => 'domainFACTORY',
	'constraints' => [
		'depends' => [
			'typo3' => '12.4.0-12.4.99',
			'php' => '8.1.0-8.3.99',
		],
		'conflicts' => [
		],
		'suggests' => [
		],
	],
];
