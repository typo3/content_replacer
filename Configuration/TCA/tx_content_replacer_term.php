<?php

// Path to the localisation file
$localisationFilePath = 'LLL:EXT:content_replacer/Resources/Private/Language/locallang_db.xlf:';
$generalLabelsPrefix = 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.';

$columns = [
	'ctrl' => [
		'title' => $localisationFilePath . 'tx_content_replacer_term',
		'label' => 'term',
		'label_alt' => 'category_uid',
		'label_alt_force' => TRUE,
		'languageField' => 'sys_language_uid',
		'transOrigPointerField' => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'versioningWS' => TRUE,
		'origUid' => 't3_origuid',
		'groupName' => 'content_replacer',
		'default_sortby' => 'ORDER BY category_uid, term',
		'delete' => 'deleted',
		'enablecolumns' => [
			'disabled' => 'hidden',
			'starttime' => 'starttime',
			'endtime' => 'endtime'
		],
		'security' => [
			'ignorePageTypeRestriction' => 1
		],
		'searchFields' => 'term, replacement, description',
		'iconfile' => 'EXT:content_replacer/Resources/Public/Icons/icon_tx_content_replacer_term.png',
	],
	'interface' => [],
	'types' => [
		0 => [
			'showitem' => 'sys_language_uid,l10n_parent,l10n_diffsource,hidden,--palette--;;1,term,category_uid,stdWrap,replacement,description,starttime,endtime',
		],
	],
	'columns' => [
		'sys_language_uid' => [
			'exclude' => TRUE,
			'label' => $generalLabelsPrefix . 'language',
			'config' => ['type' => 'language'],
		],
		'l10n_parent' => [
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'label' => $generalLabelsPrefix . 'l18n_parent',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectSingle',
				'items' => [
					['label' => '', 'value' => 0],
				],
				'foreign_table' => 'tx_content_replacer_term',
				'foreign_table_where' => 'AND tx_content_replacer_term.pid=###CURRENT_PID### ' .
					'AND tx_content_replacer_term.sys_language_uid IN (-1,0)',
			],
		],
		'l10n_diffsource' => [
			'config' => [
				'type' => 'passthrough',
			]
		],
		'hidden' => [
			'exclude' => TRUE,
			'label' => $generalLabelsPrefix . 'hidden',
			'config' => [
				'type' => 'check',
				'default' => FALSE,
			]
		],
		'starttime' => [
			'exclude' => TRUE,
			'label' => $generalLabelsPrefix . 'starttime',
			'config' => [
				'type' => 'datetime',
				'default' => 0
			]
		],
		'endtime' => [
			'exclude' => TRUE,
			'label' => $generalLabelsPrefix . 'endtime',
			'config' => [
				'type' => 'datetime',
				'default' => 0,
				'range' => [
					'upper' => mktime(0, 0, 0, 1, 1, 2038)
				]
			]
		],
		'term' => [
			'l10n_mode' => 'exclude',
			'label' => $localisationFilePath . 'tx_content_replacer_term.term',
			'config' => [
				'type' => 'input',
				'size' => 40,
				'eval' => 'trim',
				'required' => TRUE,
			]
		],
		'category_uid' => [
			'l10n_mode' => 'exclude',
			'label' => $localisationFilePath . 'tx_content_replacer_term.category_uid',
			'config' => [
				'type' => 'select',
				'renderType' => 'selectMultipleSideBySide',
				'foreign_table' => 'tx_content_replacer_category',
				'minitems' => 1,
				'maxitems' => 1,
				'fieldControl' => [
					'addRecord' => [
						'disabled' => FALSE,
						'options' => [
							'pid' => '###CURRENT_PID###',
							'table' => 'tx_content_replacer_category',
							'setValue' => 'prepend'
						]
					]
				]
			],
		],
		'stdWrap' => [
			'label' => $localisationFilePath . 'tx_content_replacer_term.stdWrap',
			'config' => [
				'type' => 'input',
				'size' => 40,
				'eval' => 'trim',
				'behaviour' => [
					'allowLanguageSynchronization' => TRUE
				]
			],
		],
		'replacement' => [
			'label' => $localisationFilePath . 'tx_content_replacer_term.replacement',
			'config' => [
				'type' => 'text',
				'enableRichtext' => TRUE,
				'cols' => 40,
				'rows' => 3,
			]
		],
		'description' => [
			'exclude' => TRUE,
			'label' => $localisationFilePath . 'tx_content_replacer_term.description',
			'config' => [
				'type' => 'text',
				'cols' => 40,
				'rows' => 3,
			],
		],
	],
];

return $columns;
