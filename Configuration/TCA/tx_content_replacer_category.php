<?php

// Path to the localisation file
$localisationFilePath = 'LLL:EXT:content_replacer/Resources/Private/Language/locallang_db.xlf:';
$generalLabelsPrefix = 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.';

$columns = [
	'ctrl' => [
		'title' => $localisationFilePath . 'tx_content_replacer_category',
		'label' => 'category',
		'tstamp' => 'tstamp',
		'crdate' => 'crdate',
		'versioningWS' => TRUE,
		'origUid' => 't3_origuid',
		'default_sortby' => 'ORDER BY category',
		'groupName' => 'content_replacer',
		'delete' => 'deleted',
		'enablecolumns' => [
			'disabled' => 'hidden'
		],
		'security' => [
			'ignorePageTypeRestriction' => 1
		],
		'searchFields' => 'category, description',
		'iconfile' => 'EXT:content_replacer/Resources/Public/Icons/icon_tx_content_replacer_category.png',
	],
	'interface' => [],
	'types' => [
		0 => [
			'showitem' => 'hidden,--palette--;;1,category,description',
		]
	],
	'palettes' => [
		0 => [
			'showitem' => '',
		],
	],
	'columns' => [
		'hidden' => [
			'exclude' => TRUE,
			'label' => $generalLabelsPrefix . 'hidden',
			'config' => [
				'type' => 'check',
				'default' => FALSE,
			],
		],
		'category' => [
			'l10n_mode' => 'exclude',
			'label' => $localisationFilePath . 'tx_content_replacer_category.category',
			'config' => [
				'type' => 'input',
				'size' => 40,
				'eval' => 'trim,unique',
				'required' => TRUE,
			],
		],
		'description' => [
			'exclude' => TRUE,
			'label' => $localisationFilePath . 'tx_content_replacer_category.description',
			'config' => [
				'type' => 'text',
				'cols' => 40,
				'rows' => 3,
			],
		],
	],
];

return $columns;
