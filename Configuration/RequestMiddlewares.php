<?php

use SGalinski\ContentReplacer\Middleware\ContentReplacerMiddleware;

return [
	'frontend' => [
		'sgalinski/content-replacer' => [
			'target' => ContentReplacerMiddleware::class,
			'description' => '',
			'before' => [
				'typo3/cms-frontend/content-length-headers'
			]
		]
	]
];
