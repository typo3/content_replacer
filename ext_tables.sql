-- -----------------------------------------------------
-- Table tx_content_replacer_term
-- -----------------------------------------------------
CREATE TABLE tx_content_replacer_term (
	term varchar(255) NOT NULL DEFAULT '',
	category_uid int(10) unsigned NOT NULL DEFAULT '0',
	stdWrap varchar(255) NOT NULL DEFAULT '',
	replacement text,
	description text,

	INDEX category (category_uid),
	INDEX terms (term)
);

-- -----------------------------------------------------
-- Table tx_content_replacer_category
-- -----------------------------------------------------
CREATE TABLE tx_content_replacer_category (
	category varchar(255) NOT NULL DEFAULT '',
	description text
);
