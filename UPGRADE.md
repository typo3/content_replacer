# Version 8 Breaking Changes

- Dropped TYPO3 10 and 11 support
- Dropped php 7 support

# Version 7 Breaking Changes

- Dropped TYPO3 9 support
- Dropped php 7.3 support

# Version 6 Breaking Changes

- Dropped TYPO3 8 support
