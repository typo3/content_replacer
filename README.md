# EXT: content_replacer

<img src="https://www.sgalinski.de/typo3conf/ext/project_theme/Resources/Public/Images/logo.svg" />

License: [GNU GPL, Version 2](https://www.gnu.org/licenses/gpl-2.0.html)

Repository: https://gitlab.sgalinski.de/typo3/content_replacer

Please report bugs here: https://gitlab.sgalinski.de/typo3/content_replacer

## Table of Contents

1. EXT: content_replacer
    1. Introduction3
    2. What does it do?
    3. Advantages vs. disadvantages
    4. The wildcard term
    5. The “stdWrap” transformation
2. Administration
    1. Setup
    2. Tagging of the replacement terms
    3. RTE integration
3. Configuration
    1. Reference
4. Known Problems

## Introduction

### What does it do?
This extension parses your content and replaces all defined terms with a flexible replacement text. Maybe you already heard and read about such extensions, but this one is diverse in a singe issue: Performance! Also please note that the extension isn't a glossary. If you need a glossary, you should install a glossary extension.

### Advantages vs. disadvantages
The main advantage of this extension is the performance that is significantly higher than the replacement algorithms of the most – maybe all – other replacement extensions. You can have hundreds of nested replacement terms without any major performance drop.
On the other side, you need to pay with an additional workload. The fast replacement algorithm works, because it's highly optimized and accompanied with a useful search string. You need to wrap any term that should be replaced with a special span tag or a custom string.

### The wildcard term
The wrapping of the substitution terms has another nice advantage, because you can replace nearly any value by using such called wildcard replacement terms. This could be useful if you want to fetch some informations from a database based on the term that is defined in the replacement wrap.
You can define one wildcard term for each category, called “*”.

### The “stdWrap” transformation
You can add a stdWrap configuration for each of your terms. If you don't add a replacement text, the replacement value inside the source code is used as the content for the stdWrap transformation.

**Examples**

The first example “readURL” applies a user function for the replacement value. The second one, “listOfPrices”, surrounds the replacements with a link. You can use any possibilities of the mighty stdWrap property.

```
plugin.tx_content_replacer {
    readURL {
       preUserFunc = user_typoscriptFunctions->myUserFunction
    }

    listOfPrices {
        typolink.parameter = 1
    }
}
```

## Administration

### Setup

Install the extension
Create a system folder to organize your terms and categories. Note that this is just an advice, because you can add categories and terms nearly everywhere.
If you configure ```plugin.tx_content_replacer.storagePid``` you need to take care of the correct location of the terms. With this configuration you are able to have different replacements for the same term in another tree branch.
Create a new category inside the folder (with “web->list”)

Create some terms in the same directory now

Afterwards you need to add the static template of this extension to your typoscript root template. It's needed to predefine some configuration values.

### Tagging of the replacement terms

After a successful installation and initialization you can start to use the functionality. Just go inside your content elements and wrap your terms with the span tag or your defined custom non-html variant. The tag must contain a class that is prefixed with the “prefix” configuration option and followed by the category name.

**Example with the category “text”**: Hi, My name is ```<span class=”replace-text”>nameTerm</span>```.

This example works instantly if you have created the replacement term “nameTerm” somewhere in your page tree. Note that the term must be in the category which is given by the class name. This is the category “text” in our example.

We gain several advantages, if we *tag* our occurrences in this way.
Special classes for categories that can be styled by CSS (e.g. in the frontend to mark non-replaced occurrences or to mark them in the RTE with a content css stylesheet)!
Easy and extremely fast way to parse the final content!
Never unwanted replacements of occurrences, for example inside the navigation!

### RTE integration

The image above demonstrates how easily you can mark terms with the ckeditor. The first two options in the collapsed format box are adding a surrounding span tag with the classes “replace-text” or “replace-feature” to the selected value in the RTE. The current marked entries are colored in red and underlined with a dashed border. This is possible, because you can load a special css file for the RTE. You will find the needed RTE configuration below.

```
editor:
  config:
    contentsCss:
      - "EXT:my_ext/Resources/Public/StyleSheets/rte.css"
    stylesSet:
      - {name: "Category1", element: "span", attributes: { class: "replace-text" }}
      - {name: "Category2", element: "span", attributes: { class: "replace-feature" }}
```

## Configuration

You will find the configuration options inside the static template. You can edit the constants with the constant editor (PLUGIN.CONTENT_REPLACER) in Web->Template.

### Reference

Here is a reference that is valid for both, global options and typoscript constants.

| Property  |  Data type |  Description | Default  |
|---|---|---|---|
|  disabled | boolean  |  Use this option to disable the replacement mechanism on some special pages or complete page branches. |  false |
| amountOfPasses |  integer |  Determines the amount of passes through the content to prevent possible endless recursions. This can happen if you define a replacement term “foo” in the replacement text “bar” that contains inside it's replacement text the replacement term “foo” again. <br><br> Note that this mechanism doesn't influences the performance, because the replacement is only continued until no further terms are found. |  5 |
|  prefix |  string |  You can set this class prefix to any value you like. It's used as a prefix for the span tags to prevent possible collisions with existing other classes. |  replace- |
| specialParserCharacter |  character |  Single character that acts a your custom non-html wrap <br><br> Example: <br> Defined Character: ~ <br> Possible Wrap: ~replace-myCategory~myTerm~~ |   |

```[plugin.tx_content_replacer]```

## Known Problems

Currently there are no known problems!
Bugtracker: https://gitlab.sgalinski.de/typo3/content_replacer/issues
